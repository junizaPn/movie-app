import React, { useEffect, useState } from 'react'
import { FlatList, StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import Update from '../../components/card'
import Recomend from '../../components/recomend'
import axios from '../../service'
// import axios from 'axios'

const Home = ({ navigation, onPress }) => {

  const [movie, setMovie] = useState([])
  // const [isLoading, setLoading] = useState(true)

  useEffect(() => {
    getDataFromAPI()
  }, [])

  // const homeResponse = async () => {
  //   const res = await Service.get('v1/movies')
  //   console.log(res.movie)
  // }

  function getDataFromAPI() {
    // setLoading(true)
    axios.get('v1/movies').then(function (res) {
      setMovie(res.data.results)
    })
      .catch(function (error) {
        console.log(error)
      })
    // .finally(() => {
    //   setLoading(false)
    // })
  }

  if (!movie) {
    return null
  }

  function Recommended(a, b) {
    return parseFloat(b.popularity) - parseFloat(a.popularity)
  }

  function latest_upload(a, b) {
    return new Date(b.release_date).getTime() - new Date(a.release_date).getTime()
  }

  function renderRecomend() {
    return (
      <>
        <Text style={styles.text}>Recommended</Text>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator= {false}
          data={movie.sort(Recommended)}
          keyExtractor={(item, index) => 'key' + index}
          renderItem={({ item }) => {
            return (
              <View>
                <Recomend id={item.id} onPress={() => navigation.navigate('MovieScreen', { id: item.id })} data={item} />
              </View>
            )
          }}
        />
        <Text style={styles.text}>Latest Upload</Text>
      </>
    )
  }

  return (
    <View style={{ backgroundColor: '#99ccff' }}>
      <FlatList
        ListHeaderComponent={renderRecomend}
        data={movie.sort(latest_upload)}
        keyExtractor={(item, index) => 'key' + index}
        renderItem={({ item }) => {
          return (
            <View>
              {/* <Recomend item={item} /> */}
              <Update id={item.id} onPress={() => navigation.navigate('MovieScreen', { id: item.id })} item={item} />
            </View>
          )
        }}
      />
    </View>
  )
}

export default Home

const styles = StyleSheet.create({
  // container: {
  //   flexDirection: 'row',
  //   flex: 1,
  //   backgroundColor: '#fff',
  //   borderRadius: 5,
  //   height: 100
  // },
  // containerLast: {
  //   flexDirection: 'column',
  //   backgroundColor: '#fff',
  //   borderRadius: 5,
  // },
  text: {
    marginVertical: 10,
    marginTop: 10,
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginLeft: 13,
    color: 'white',
    fontSize: 18,
    fontWeight: 'bold'
  },
  // LastText: {
  //   flexDirection: 'row',
  //   alignSelf: 'flex-end'
  // }
})