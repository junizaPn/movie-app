import { StyleSheet, Text, View, SafeAreaView } from 'react-native'
import React, { useState, useEffect } from 'react'
import ListMovieDetail from '../../components/list'
import axios from '../../service'
import Icon from 'react-native-vector-icons/MaterialIcons'

const MovieScreen = ({ route, navigation }) => {
    const { id } = route.params
    const [data, setData] = useState([])
    const [isLoading, setLoading] = useState(true)

    useEffect(() => {
        getDataFromAPI()
    }, [id])

    function getDataFromAPI() {
        console.log(id)
        setLoading(true)
        axios.get(`v1/movies/${id}`).then(function (res) {
            setData(res.data)
        })
            .catch(function (error) {
                console.log(error)
            }).finally(() => {
                setLoading(false)
            })
    }

    return (
        <View>
            <View style={styles.header}>
                <View>
                    <Icon.Button
                        name="arrow-back"
                        onPress={() => navigation.goBack()}
                    />
                </View>
                <View style={{marginLeft: 220}}>
                    <Icon.Button
                        name="favorite"
                        color="white"
                        // onPress={() => navigation.goBack()}
                    />
                </View>
                <View style={{marginLeft: 20}}>
                    <Icon.Button
                        name="share"
                        color="white"
                        // onPress={() => navigation.goBack()}
                    />
                </View>
            </View>
            { !isLoading && <ListMovieDetail item={data} />}
        </View>
    )
}
export default MovieScreen

const styles = StyleSheet.create({
    header: {
        marginLeft: 10,
        marginTop: 5,
        width: '100%',
        display: 'flex',
        flexDirection: 'row',
        backgroundColor: '#'
    }
})