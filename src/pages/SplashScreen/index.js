import { StyleSheet, Text, View, ImageBackground } from 'react-native'
import React, { useEffect } from 'react'
import {SplashImage} from '../../assets'

const SplashScreen = ({ navigation }) => {

  useEffect(() => {
    setTimeout( () => {
      navigation.replace('Home');
    }, 3000)
  }, [navigation]);

  return (
    <ImageBackground source={SplashImage} style={styles.background}>
    </ImageBackground>
  )
}

export default SplashScreen

const styles = StyleSheet.create({
  background: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
})