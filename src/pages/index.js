import Home from "./HomeScreen";
import Splash from "./SplashScreen";
import Movie from "./MovieScreen"

export {Home, Splash, Movie}