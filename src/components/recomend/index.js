import { StyleSheet, Text, View, Image, ScrollView, FlatList, TouchableOpacity } from 'react-native';
import React, { useState } from 'react';

const Recomend = ({ data, onPress }) => {
    // const onPress = () => (navigation.navigate('MovieScreen', { id: item.id }))
    return (
        <View>
            <View >
                <TouchableOpacity onPress={onPress}>
                    <Image source={{ uri: data.poster_path }} style={styles.image} />
                </TouchableOpacity>
            </View>
        </View>
    );
}

export default Recomend

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flex: 1,
        backgroundColor: '#fff',
        height: 200
    },
    text: {
        marginVertical: 10,
        justifyContent: 'space-between',
        flexDirection: 'row',
        marginLeft: 13,
        color: 'white',
        fontSize: 15,
    },
    image: {
        width: 150,
        height: 200,
        backgroundColor: 'white',
        marginHorizontal: 13,
        marginVertical: 5,
        borderRadius: 5,
        alignSelf: 'center'
    },
})