import { StyleSheet, Text, View, Image, ImageBackground, ScrollView } from 'react-native';
import React, { useState, useEffect } from 'react';
import { SplashImage, Sample } from '../../assets';
// import axios from '../../service'
import Genre from '../genre';
// import ImageActor from '../imageActor';
import ListActor from '../listActor';

const ListMovieDetail = ({ item, id }) => {


  // console.log("test "+JSON.stringify(item.credits.cast))


  return (
    <View style={styles.container}>
      <ImageBackground source={{ uri: item.backdrop_path }} style={styles.ImageBanner}>
        <View>
          <View style={styles.box}>
            <Image source={{ uri: item.poster_path }} style={styles.ImageSample} />

            <View style={{ flexDirection: 'column', marginRight: 10, marginLeft: 10 }}>
              <Text>{item.original_title}</Text>
              <Text style={{ marginTop: 10, marginRight: 70 }}>{item.tagline}</Text>
              <Text style={{ marginTop: 10 }}>{item.status}</Text>
              <View style={{ flexDirection: 'row', marginTop: 10 }}>
                <Text>{item.release_date}</Text>
                <Text style={styles.text}>{item.vote_average}</Text>
                <Text style={styles.text}>{item.runtime}</Text>
              </View>
            </View>
          </View>
          <View>
            <ScrollView vertical={true} showsVerticalScrollIndicator={false} >
            <Text style={{ marginTop: 30, marginHorizontal: 17, fontWeight: 'bold' }}>Genres</Text>
            <View style={{ flexDirection: 'row' }}>
              {item.genres.map((genre) => {
                return <Genre key={genre.id} genre={genre.name}></Genre>
              })}
            </View>
            <View style={{ marginTop: 10, marginRight: 120, marginLeft: 17 }}>
              <Text style={{ fontWeight: 'bold' }}>Synopshis</Text>
              <Text >{item.overview}</Text>
            </View>

            <Text style={{ marginTop: 30, marginHorizontal: 17, fontWeight: 'bold' }}>Actors/Artist</Text>
            <View style={{ flexDirection: 'row' }}>
              {item.credits.cast.map((img) => {
                return <Image key={img.id} source={{ uri: img.profile_path }} style={styles.ImageDown}></Image>
              })}
            </View>
            <View style={{ flexDirection: 'row', marginHorizontal: 17, marginTop: 10 }}>
              {item.credits.cast.map((list) => {
                return <ListActor key={list.id} listName={list.name} ></ListActor>
              })}
            </View>
            </ScrollView>
          </View>
          {/* <View style={{ flexDirection: 'row' }}>
            <Image source={Sample} style={styles.ImageDown} />
            <Image source={Sample} style={styles.ImageDown} />
            <Image source={Sample} style={styles.ImageDown} />
          </View>
          <View
            style={{ flexDirection: 'row', marginHorizontal: 17, marginTop: 10 }}>
            <Text style={{ alignSelf: 'center', marginLeft: 20 }}>Image 4</Text>
            <Text style={{ marginLeft: 80, alignSelf: 'center' }}>Image 5</Text>
            <Text style={{ marginLeft: 80, alignSelf: 'center' }}>Image 6</Text>
          </View> */}

        </View>
      </ImageBackground>
    </View >
  );
};

export default ListMovieDetail;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  ImageBanner: {
    height: 200,
    width: 500,
    justifyContent: 'center',
  },
  box: {
    width: 362,
    height: 150,
    padding: 17,
    marginHorizontal: 15,
    marginTop: 400,
    backgroundColor: 'white',
    borderRadius: 5,
    flexDirection: 'row',
    elevation: 20
  },
  ImageSample: {
    width: 100,
    height: 120,
    borderRadius: 10,
  },
  text: {
    marginLeft: 20,
  },
  ImageDown: {
    width: 96,
    height: 120,
    marginTop: 20,
    marginHorizontal: 17,
    borderRadius: 10,
  },
})
