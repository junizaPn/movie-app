import { View, Text, StyleSheet } from 'react-native'
import React from 'react'

const ListActor = ({ listName }) => {

    return (
        <View style={{flexDirection: 'row'}}>
            <Text style={{ marginLeft: 20, alignSelf: 'center' }}>{listName}</Text>
        </View>
    )
}

export default ListActor
const styles = StyleSheet.create({
    text1: {
        color: 'black',
        fontSize: 15,
        marginLeft: 17,
        marginTop: 10,
        marginBottom: 10,
        alignSelf: 'auto',
        fontFamily: 'Helvetica',
    },
})