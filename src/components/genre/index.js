import { View, Text, StyleSheet } from 'react-native'
import React from 'react'

const Genre = ({ genre }) => {

    return (
        <View style={{flexDirection: 'row'}}>
            <Text style={styles.text1}>{genre}</Text>
        </View>
    )
}

export default Genre
const styles = StyleSheet.create({
    text1: {
        color: 'black',
        fontSize: 15,
        marginLeft: 17,
        marginTop: 10,
        marginBottom: 10,
        alignSelf: 'auto',
        fontFamily: 'Helvetica',
    },
})