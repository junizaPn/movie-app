import { StyleSheet, Text, View, Image, Button, ScrollView } from 'react-native'
import React, { useState, useEffect } from 'react'
import Genre from '../genre';
import axios from '../../service'


const Update = ({ item, onPress, id }) => {
  const [data, setData] = useState([])

  useEffect(() => {
    axios.get(`v1/movies/${id}`).then(function (res) {
      setData(res.data.genres)
      // console.log(res.data)
    })
  }, [])
  
  return (
    <View>
      <View style={{ flexDirection: 'row' }}>
        <ScrollView showsVerticalScrollIndicator={false} >
          <View style={{ flexDirection: 'row' }} >
            <Image source={{ uri: item.poster_path }} style={styles.Image} />
            <View style={{marginRight: 200}}>
              <Text style={styles.text1}>{item.original_title}</Text>
              <Text style={styles.text1}>Date: {item.release_date}</Text>
              <Text style={styles.text1}>
                {item.vote_average}/10
              </Text>
              <View>
                {data.map((item) => {
                  return <Genre key={item.id} genre={item.name}></Genre>
                })}
              </View>
              
              <Button onPress={onPress} title="Show More" color='red' style={{width: 20}} />
            </View>
          </View>
        </ScrollView>
      </View>
    </View>
  )
}

export default Update

const styles = StyleSheet.create({
  imageP: {
    width: 15,
    height: 20,
    marginVertical: 13,
    flexDirection: 'row'
  },
  containerLast: {
    flexDirection: 'row',
    backgroundColor: '#fff',
    borderRadius: 5,
  },
  LastText: {
    flexDirection: 'row',
    alignSelf: 'flex-end'
  },
  Image: {
    width: 150,
    height: 200,
    backgroundColor: 'white',
    marginHorizontal: 13,
    marginTop: 10,
  },
  text1: {
    color: 'black',
    fontSize: 15,
    marginLeft: 17,
    marginTop: 10,
    marginBottom: 10,
    // alignSelf: 'auto',
    fontFamily: 'Helvetica',
  },
})