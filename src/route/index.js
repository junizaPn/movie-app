import { StyleSheet } from 'react-native';
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Home, Splash, Movie} from '../pages';


const Stack = createNativeStackNavigator();

const Route = () => {
    return (
        <Stack.Navigator initialRouteName='Splash' screenOptions={{
            headerShown: false
        }}>
            <Stack.Screen name="Splash" component={Splash} />
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="MovieScreen" component={Movie} />
        </Stack.Navigator>
    )
}

export default Route

const styles = StyleSheet.create({})